import { Component } from '@angular/core';
import { ZoneService } from '../../services/zone.service';
import { UserService } from '../../services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { ZoneDialogComponent } from '../zone-dialog/zone-dialog.component';
import { IZone } from '../../interfaces';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'app-zones',
    templateUrl: './zones.component.html',
    styleUrls: ['./zones.component.scss']
})
export class ZonesComponent {
    id = this._userService.id;
    query$ = new Subject<string>();
    zones$: Observable<IZone[]> = combineLatest(
        this._zoneService.getZones(), this.query$.pipe(startWith('')),
    ).pipe(
        map(([zones, query]) => {
            query = query.trim().toLowerCase();
            return (zones || []).filter(zone => zone.name.toLowerCase().includes(query))
        })
    );
    isAdmin = this._userService.isAdmin;

    constructor(private _zoneService: ZoneService,
                private _userService: UserService,
                private _dialog: MatDialog) {}

    openAddDialog() {
        this._dialog.open(ZoneDialogComponent);
    }

    edit(zone: IZone) {
        this._dialog.open(ZoneDialogComponent, {
            data: zone
        })
    }

    delete(zoneId: string) {
        this._zoneService.removeZone(zoneId).subscribe();
    }
}
