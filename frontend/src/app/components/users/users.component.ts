import { Component } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ROLE_MAP } from '../../utils/maps';
import { MatDialog } from '@angular/material/dialog';
import { IUser } from '../../interfaces';
import { UserDialogComponent } from '../user-dialog/user-dialog.component';
import { UserRoleE } from '../../enums';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent {
    readonly rolesMap = Object.freeze(ROLE_MAP);
    users$ = this._userService.getUsers();
    id = this._userService.id;
    isSuperUser = this._userService.role === UserRoleE.SUPER_USER;

    constructor(private _userService: UserService,
                private _dialog: MatDialog) {}

    addUser() {
        this._dialog.open(UserDialogComponent);
    }

    deleteUser(userId: string) {
        this._userService.deleteUser(userId).subscribe();
    }

    editUser(user: IUser) {
        this._dialog.open(UserDialogComponent, {
            data: user
        });
    }
}
