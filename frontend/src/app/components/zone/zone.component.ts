import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { interval, Observable } from 'rxjs';
import { IZone } from '../../interfaces';
import { map, startWith, tap, withLatestFrom } from 'rxjs/operators';
import { ZoneService } from '../../services/zone.service';

@Component({
    selector: 'app-zone',
    templateUrl: './zone.component.html',
    styleUrls: ['./zone.component.scss']
})
export class ZoneComponent implements OnInit {
    zone$: Observable<IZone> = this._route.data.pipe(
        map(({ zone }) => zone),
        tap(zone => {
            this.diff = (zone.offset * 1000 - this._zoneService.browserOffset) / 1000;
        }));
    time$: Observable<Date>;
    diff: number;

    constructor(private _zoneService: ZoneService,
                private _route: ActivatedRoute) {}

    ngOnInit() {
        this.time$ = interval(1000).pipe(
            startWith(null),
            withLatestFrom(this.zone$),
            map(([_, zone]) => {
                const now = new Date();
                const timestamp = now.getTime() - this._zoneService.browserOffset + zone.offset * 1000;
                return new Date(timestamp);
            }));
    }
}
