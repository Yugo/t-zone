import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IUser } from '../../interfaces';
import { UserService } from '../../services/user.service';
import { UserRoleE } from '../../enums';
import { startWith, takeUntil } from 'rxjs/operators';
import { ROLE_MAP } from '../../utils/maps';

@Component({
    selector: 'app-user-dialog',
    templateUrl: './user-dialog.component.html',
    styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit, OnDestroy {
    passwordVisible = false;
    options = Object.keys(ROLE_MAP).map(key => ({ label: ROLE_MAP[key], key }));
    private _destroy$ = new Subject();
    isSuperUser = this._userService.role === UserRoleE.SUPER_USER;
    invalidSubmitMessage$ = new BehaviorSubject<string>(null);
    disabledButton$ = new BehaviorSubject<boolean>(true);

    form = new FormGroup({
        name: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('')
    });

    constructor(private _userService: UserService,
                private _dialogRef: MatDialogRef<UserDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public user: IUser) {}

    ngOnInit() {
        if (this.isSuperUser) {
            this.form.addControl('role', new FormControl(UserRoleE.REGULAR));
        }

        if (this.user) {
            const value = {
                name: this.user.name,
                email: this.user.email,
                password: '',
                role: this.user.role
            };
            if (!this.isSuperUser) delete value.role;
            this.form.setValue(value, { emitEvent: false });
            this.form.controls.email.disable( { emitEvent: false });
        } else {
            this.form.controls.password.setValidators([
                Validators.required,
                Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$/)
            ]);
        }

        this.form.valueChanges
            .pipe(
                startWith(this.form.value),
                takeUntil(this._destroy$))
            .subscribe(value => {
                this.invalidSubmitMessage$.next(null);
                let disabled;
                if (this.form.invalid) disabled = true;
                else if (!this.user) disabled = false;
                else disabled = Object.keys(this.form.controls)
                    .filter(key => key !== 'password')
                    .every(key => this.user[key] === value[key]) &&
                    (this.form.value.password
                        && !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$/.test(this.form.value.password))
                this.disabledButton$.next(disabled);
            });
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }

    submit() {
        const user = { ...this.form.value };
        if (user && !user.password) delete user.password;
        const request = this.user
            ? this._userService.updateUser(this.user._id, user)
            : this._userService.addUser(user);

        request.subscribe(
            () => this._dialogRef.close(),
            error => this.invalidSubmitMessage$.next(error.error));
    }
}
