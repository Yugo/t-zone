import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ZoneService } from '../../services/zone.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, map, startWith, takeUntil, withLatestFrom } from 'rxjs/operators';
import { IZone, IZoneOption } from '../../interfaces';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-zone-dialog',
    templateUrl: './zone-dialog.component.html',
    styleUrls: ['./zone-dialog.component.scss']
})
export class ZoneDialogComponent implements OnInit, OnDestroy {
    private _destroy$ = new Subject();
    private _search$ = new Subject<string>();
    invalidSubmitMessage$ = new BehaviorSubject<string>(null);
    disabledButton$ = new BehaviorSubject<boolean>(true);

    form = new FormGroup({
        name: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        offset: new FormControl(null, Validators.required)
    });

    options$ = this._search$.pipe(
        startWith(''),
        withLatestFrom(this._zoneService.getOptions()),
        filter(([query]) => typeof query === 'string'),
        map(([query, options]: [string, IZoneOption[]]) => {
            if (!query) return options;
            query = query.trim().toLowerCase();
            return options.filter(option =>
                option.zoneName
                    .toLowerCase()
                    .split('_')
                    .join(' ')
                    .includes(query));
        })
    )

    constructor(private _zoneService: ZoneService,
                private _dialogRef: MatDialogRef<ZoneDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public zone: IZone) {}

    ngOnInit() {
        if (this.zone) {
            this.form.setValue({
                name: this.zone.name,
                city: this.zone.city,
                offset: this.zone.offset
            }, { emitEvent: false });
        }
        this.form.valueChanges
            .pipe(startWith(this.form.value), takeUntil(this._destroy$))
            .subscribe(value => {
                this.invalidSubmitMessage$.next(null);
                let disabled;
                if (this.form.invalid) disabled = true;
                else if (!this.zone) disabled = false;
                else disabled = Object.keys(this.form.controls).every(key => this.zone[key] === value[key]);
                this.disabledButton$.next(disabled);
            });

        this.form.controls.city.valueChanges
            .pipe(takeUntil(this._destroy$))
            .subscribe(value => {
                this._search$.next(value);
                this.form.controls.offset.setValue(null, { emitEvent: false });
            });
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }

    submit() {
        const request = this.zone
            ? this._zoneService.updateZone(this.zone._id, this.form.value)
            : this._zoneService.addZone(this.form.value);

        request.subscribe(
            () => this._dialogRef.close(),
            error => this.invalidSubmitMessage$.next(error.error));
    }

    onFocus() {
        this._search$.next('');
    }

    onSelect(zone: IZoneOption) {
        this.form.controls.city.setValue(zone.zoneName);
        this.form.controls.offset.setValue(zone.gmtOffset);
        this._search$.next('');
    }
}
