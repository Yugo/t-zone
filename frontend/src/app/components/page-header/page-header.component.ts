import { Component } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent {
    constructor(private _userService: UserService) {}

    get isAuthorized(): boolean {
        return this._userService.isAuthorized;
    }

    get canManageUsers(): boolean {
        return this._userService.canManageUsers;
    }

    get canManageZones(): boolean {
        return this._userService.canManageZones;
    }

    logout() {
        this._userService.logout();
    }

}


