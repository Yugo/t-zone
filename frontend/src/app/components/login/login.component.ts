import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['../form.component.scss', './login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    private _destroy$ = new Subject();
    passwordVisible = false;
    invalidSubmitMessage$ = new BehaviorSubject<string>(null);

    loginForm = new FormGroup({
        email: new FormControl('', [
            Validators.required,
            Validators.email
        ]),
        password: new FormControl('', [
            Validators.required,
            Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$/)
        ])
    });

    constructor(private _userService: UserService) {}

    ngOnInit() {
        this.loginForm.valueChanges
            .pipe(
                takeUntil(this._destroy$),
                filter(() => !!this.invalidSubmitMessage$.value)
            )
            .subscribe(() => this.invalidSubmitMessage$.next(null));
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }

    submit() {
        this._userService.login(this.loginForm.value)
            .subscribe(
            () => {},
            error => {
                this.invalidSubmitMessage$.next(error.error);
            });
    }
}
