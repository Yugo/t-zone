import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['../form.component.scss', './signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
    private _destroy$ = new Subject();
    passwordVisible = false;
    invalidSubmitMessage$ = new BehaviorSubject<string>(null);

    signupForm = new FormGroup({
        name: new FormControl('', [Validators.required]),
        email: new FormControl('', [
            Validators.required,
            Validators.email
        ]),
        password: new FormControl('', [
            Validators.required,
            Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$/)
        ])
    });

    constructor(private _userService: UserService) {}

    ngOnInit() {
        this.signupForm.valueChanges
            .pipe(
                takeUntil(this._destroy$),
                filter(() => !!this.invalidSubmitMessage$.value)
            )
            .subscribe(() => this.invalidSubmitMessage$.next(null));
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }

    submit() {
        this._userService.signup(this.signupForm.value)
            .subscribe(
                () => {},
                error => {
                    this.invalidSubmitMessage$.next(error.error);
                });
    }
}
