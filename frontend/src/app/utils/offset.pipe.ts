import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'offset'
})
export class OffsetPipe implements PipeTransform {
    transform(value: number, label?: string): string {
        label = label ? label + ' ' : '';
        const prefix = `${value > 0 ? '+' : value === 0 ? '±' : '-'}`;
        let formatted: any = String(Math.abs(value / 3600));
        let leadingZero = formatted >= 10 ? '' : '0';
        let suffix;
        const fraction = String(formatted).split('.')[1];
        if (!fraction) suffix = ':00';
        else if (fraction.charAt(0) === '2') suffix = ':15';
        else if (fraction.charAt(0) === '5') suffix = ':30';
        else if (fraction.charAt(0) === '7') suffix = ':45';

        return label + prefix + leadingZero + parseInt(formatted) + suffix;
    }
}
