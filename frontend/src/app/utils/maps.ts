import { UserRoleE } from '../enums';

export const ROLE_MAP = {
    [UserRoleE.MANAGER]: 'User Manager',
    [UserRoleE.REGULAR]: 'Regular User',
    [UserRoleE.ADMIN]: 'Administrator',
    [UserRoleE.SUPER_USER]: 'Super User',
}