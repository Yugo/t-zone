export enum UserRoleE {
    SUPER_USER = 'super_user',
    ADMIN = 'admin',
    MANAGER = 'manager',
    REGULAR = 'regular'
}