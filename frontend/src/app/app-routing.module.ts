import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SignupComponent } from './components/signup/signup.component';
import { AuthGuard, UnauthGuard } from './services/auth.guard';
import { ZonesComponent } from './components/zones/zones.component';
import { ZoneComponent } from './components/zone/zone.component';
import { ZoneResolver } from './services/zone.resolver';
import { UsersComponent } from './components/users/users.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [UnauthGuard]
    },
    {
        path: 'signup',
        component: SignupComponent,
        canActivate: [UnauthGuard]
    },
    {
        path: 'users',
        component: UsersComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'zones',
        component: ZonesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'zones/:id',
        component: ZoneComponent,
        canActivate: [AuthGuard],
        resolve: { zone: ZoneResolver }
    },
    {
        path: '**',
        component: PageNotFoundComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
