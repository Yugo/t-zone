import { Injectable } from '@angular/core';
import { IZone } from '../interfaces';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { ZoneService } from './zone.service';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ZoneResolver implements Resolve<IZone> {
    constructor(private _zoneService: ZoneService, private _router: Router) {}

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IZone | null> {
        return this._zoneService.getZoneById(route.params.id).pipe(
            take(1),
            mergeMap(zone => {
                if (zone) return of(zone);
                else {
                    this._router.navigateByUrl('/zones');
                    return EMPTY;
                }
            })
        );
    }
}
