import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserRoleE } from '../enums';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private _userService: UserService,
                private _router: Router) {}

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this._userService.checkAuthorized().pipe(
            tap((value) => {
                if (!value) {
                    this._router.navigateByUrl('/login');
                } else if (route.url[0].path === 'zones' && !this._userService.canManageZones) {
                    this._router.navigateByUrl('/users');
                } else if (route.url[0].path === 'users' && !this._userService.canManageUsers) {
                    this._router.navigateByUrl('/zones');
                }
            })
        );
    }
}

@Injectable({
    providedIn: 'root'
})
export class UnauthGuard implements CanActivate {
    constructor(private _userService: UserService,
                private _router: Router) {}

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this._userService.checkAuthorized().pipe(
            map(value => !value),
            tap(value => {
                if (!value) {
                    const path = this._userService.role === UserRoleE.MANAGER ? '/users' : '/zones';
                    this._router.navigateByUrl(path);
                }
            })
        );
    }
}
