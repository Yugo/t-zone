import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONSTANTS } from '../utils/constants';
import { UserService } from './user.service';
import { filter, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private _userService: UserService,
                private _router: Router) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem(CONSTANTS.STORAGE_TOKEN_KEY);
        if (token) {
            req = req.clone({
                setHeaders: { Authorization: 'Bearer ' + token }
            })
        }

        return next.handle(req)
            .pipe(
                tap(event => {
                    if (event instanceof HttpResponse && event.status === 401) {
                        this._userService.cleanUserData();
                        this._router.navigateByUrl('/login');
                    }
                })
            );
    }
}
