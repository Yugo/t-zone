import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { IZone, IZoneOption } from '../interfaces';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ZoneService {
    private _zones$ = new BehaviorSubject<IZone[]>(null);
    private _zoneOptions$ = new BehaviorSubject<IZoneOption[]>(null);
    browserOffset = new Date().getTimezoneOffset() * 1000 * 60 * -1;

    constructor(private _apiService: ApiService) {}

    getZoneById(id: string): Observable<IZone> {
        return this.getZones().pipe(
            map(zones => zones.find(zone => zone._id === id)))
    }

    getZones(): Observable<IZone[]> {
        if (this._zones$.getValue()) {
            return this._zones$;
        } else {
            return this._apiService.getZones().pipe(
                tap(zones => this._zones$.next(zones)),
                switchMap(() => this._zones$));
        }
    }

    getOptions(): Observable<IZoneOption[]> {
        if (this._zoneOptions$.getValue()) {
            return this._zoneOptions$;
        } else {
            return this._apiService.getZoneDict().pipe(
                tap(dict => {
                    this._zoneOptions$.next(dict.sort((a, b) => a.gmtOffset - b.gmtOffset));
                }));
        }
    }

    addZone(body): Observable<IZone> {
        return this._apiService.createZone(body).pipe(
            tap(zone => {
                this._zones$.next([...this._zones$.value, zone]);
            })
        );
    }

    updateZone(id: string, body): Observable<IZone> {
        return this._apiService.updateZone(id, body).pipe(
            tap(updatedZone => {
                const zones = [];
                this._zones$.value.forEach(zone => {
                    if (zone._id === updatedZone._id) zones.push(updatedZone);
                    else zones.push(zone);
                });
                this._zones$.next(zones);
            })
        );
    }

    removeZone(zoneId: string) {
        return this._apiService.deleteZone(zoneId).pipe(
            tap(zone => {
                this._zones$.next(this._zones$.value.filter(zone => zone._id !== zoneId));
            })
        );
    }

    clearZones() {
        this._zones$.next(null);
    }
}
