import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ILoginResponse, IUser, IZone, IZoneOption } from '../interfaces';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private _http: HttpClient) {}

    signup(body: { name: string; email: string; password: string }): Observable<ILoginResponse> {
        return this._http.post<ILoginResponse>(`${environment.api}/users/signup`, body);
    }
  
    login(body: { email: string; password: string }): Observable<ILoginResponse> {
        return this._http.post<ILoginResponse>(`${environment.api}/users/login`, body);
    }

    logout(): Observable<null> {
        return this._http.get<null>(`${environment.api}/users/logout`);
    }

    getMe(): Observable<IUser> {
        return this._http.get<IUser>(`${environment.api}/users/me`);
    }

    getUserById(id: string): Observable<IUser> {
        return this._http.get<IUser>(`${environment.api}/users/${id}`);
    }

    getUsers(): Observable<IUser[]> {
        return this._http.get<IUser[]>(`${environment.api}/users`);
    }

    addUser(user: IUser & { password?: string }): Observable<IUser> {
        return this._http.post<IUser>(`${environment.api}/users`, user);
    }

    updateUser(userId: string, user: IUser & { password?: string }): Observable<IUser> {
        return this._http.patch<IUser>(`${environment.api}/users/${userId}`, user);
    }

    deleteUser(userId: string): Observable<IUser> {
        return this._http.delete<IUser>(`${environment.api}/users/${userId}`);
    }

    getZoneDict(): Observable<IZoneOption[]> {
        return this._http.get<IZoneOption[]>(`${environment.api}/dict/zones`);
    }

    getZones(): Observable<IZone[]> {
        return this._http.get<IZone[]>(`${environment.api}/zones`);
    }

    getZoneById(id: string): Observable<IZone> {
        return this._http.get<IZone>(`${environment.api}/zones/${id}`);
    }

    createZone(body: { name: string; city: string; offset: number }): Observable<IZone> {
        return this._http.post<IZone>(`${environment.api}/zones`, body);
    }

    updateZone(id: string, body: { name?: string; city?: string; offset?: number }): Observable<IZone> {
        return this._http.patch<IZone>(`${environment.api}/zones/${id}`, body);
    }

    deleteZone(id: string): Observable<IZone> {
        return this._http.delete<IZone>(`${environment.api}/zones/${id}`);
    }
}
