import { Injectable } from '@angular/core';
import { ILoginResponse, IUser, IZone } from '../interfaces';
import { UserRoleE } from '../enums';
import { ApiService } from './api.service';
import { Router } from '@angular/router';
import { catchError, mapTo, switchMap, tap } from 'rxjs/operators';
import { CONSTANTS } from '../utils/constants';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ZoneService } from './zone.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private _users$ = new BehaviorSubject<IUser[]>(null);
    private _user: IUser;

    constructor(private _apiService: ApiService,
                private _zoneService: ZoneService,
                private _router: Router) {}

    get isAuthorized(): boolean {
        return !!this._user;
    }

    get canManageUsers(): boolean {
        return this._user.role !== UserRoleE.REGULAR;
    }

    get canManageZones(): boolean {
        return this._user.role !== UserRoleE.MANAGER;
    }

    checkAuthorized(): Observable<boolean> {
        if (this.isAuthorized) {
            return of(true);
        }

        const token = localStorage.getItem(CONSTANTS.STORAGE_TOKEN_KEY);
        if (token) {
            return this._apiService.getMe().pipe(
                tap(user => this.setUser(user)),
                mapTo(true),
                catchError(err => {
                    this.cleanUserData();
                    return of(false);
                }));
        } else {
            return of(false);
        }
    }

    signup(body): Observable<ILoginResponse> {
        return this._apiService.signup(body).pipe(
            tap(response => {
                localStorage.setItem(CONSTANTS.STORAGE_TOKEN_KEY, response.token);
                this.setUser(response.user);
                this._router.navigateByUrl('/zones');
            })
        );
    }

    login(body: { email: string; password: string }): Observable<ILoginResponse> {
        return this._apiService.login(body).pipe(
            tap(response => {
                localStorage.setItem(CONSTANTS.STORAGE_TOKEN_KEY, response.token);
                this.setUser(response.user);
                this._router.navigateByUrl('/zones');
            })
        );
    }

    logout() {
        this._apiService.logout()
            .pipe(
                tap(() => {
                    this.cleanUserData();
                    this._zoneService.clearZones();
                    this._router.navigateByUrl('login');
                }),
            )
            .subscribe();
    }

    getUsers(): Observable<IUser[]> {
        if (this._users$.value) {
            return this._users$;
        } else {
            return this._apiService.getUsers().pipe(
                tap(users => this._users$.next(users)),
                switchMap(() => this._users$));
        }

    }

    addUser(user): Observable<IUser> {
        return this._apiService.addUser(user).pipe(
            tap(user => {
                this._users$.next([...this._users$.value, user]);
            })
        );
    }

    updateUser(id: string, body): Observable<IUser> {
        return this._apiService.updateUser(id, body).pipe(
            tap(updatedUser => {
                const users = [];
                this._users$.value.forEach(user => {
                    if (user._id === updatedUser._id) users.push(updatedUser);
                    else users.push(user);
                });
                this._users$.next(users);
            })
        );
    }

    deleteUser(userId: string): Observable<IUser> {
        return this._apiService.deleteUser(userId).pipe(
            tap(user => {
                this._users$.next(this._users$.value.filter(user => user._id !== userId));
                this._zoneService.clearZones();
            })
        );
    }

    setUser(user: IUser) {
        this._user = user;
    }

    cleanUserData() {
        localStorage.removeItem(CONSTANTS.STORAGE_TOKEN_KEY);
        this.setUser(null);
    }

    get id(): string {
        return this._user && this._user._id;
    }

    get role(): UserRoleE {
        return this._user.role;
    }

    get isAdmin(): boolean {
        return Boolean(this._user &&
            (this._user.role === UserRoleE.ADMIN || this._user.role === UserRoleE.SUPER_USER));
    }
}
