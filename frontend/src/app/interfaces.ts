import { UserRoleE } from './enums';

export interface IUser {
    _id?: string;
    role: UserRoleE,
    name: string;
    email: string;
}

export interface ILoginResponse {
    user: IUser;
    token: string;
}

export interface IZone {
    _id: string;
    name: string;
    city: string;
    offset: number;
    author: string;
}

export interface IZoneOption {
    countryCode: string;
    countryName: string;
    zoneName: string;
    gmtOffset: number;
    timestamp: number;
}