const jwt = require('jsonwebtoken');
const { ROLES } = require('../utils/enums');
const User = require('../models/User');

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, process.env.JWT_SECRET);

        const user = await User.findOne({ _id: decoded._id, 'tokens': token });

        if (!user) throw new Error();

        req.token = token;
        req.user = user;
        req.isUserAdmin = user.role === ROLES.ADMIN
            || user.role === ROLES.MANAGER
            || user.role === ROLES.SUPER_USER;
        next();
    } catch (e) {
        res.sendStatus(401);
    }
}

module.exports = auth;