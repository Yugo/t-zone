const ROLES = {
    SUPER_USER: 'super_user',
    ADMIN: 'admin',
    MANAGER: 'manager',
    REGULAR: 'regular',
}

module.exports = {
    ROLES
};