const express = require('express');
const logger = require('morgan');
const cors = require('cors');
require('./db/mongoose');

const app = express();

const userRouter = require('./api/users');
const zoneRouter = require('./api/zones');
const dictRouter = require('./api/dictionaries');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());

app.use('/users', userRouter);
app.use('/zones', zoneRouter);
app.use('/dict', dictRouter);

module.exports = app;
