const mongoose = require('mongoose');

const zoneSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    offset: {
        type: Number,
        required: true,
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
}, {
    timestamps: true
});

zoneSchema.statics.findByAuthorId = (authorId, zoneId) => {
    if (!authorId) {
        throw new Error('Author is not provided');
    }

    if (zoneId) {
        return Zone.findOne({
            author: authorId,
            _id: new mongoose.Types.ObjectId(zoneId)
        });
    } else {
        return Zone.find({ author: authorId });
    }
}

const Zone = mongoose.model('Zone', zoneSchema);

module.exports = Zone;