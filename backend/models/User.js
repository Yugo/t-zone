const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Zone = require('./Zone');
const { ROLES } = require('../utils/enums');

const userSchema = mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        validate(email) {
            if (!validator.isEmail(email)) {
                throw new Error('Invalid email');
            }
        }
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        trim: true,
        validate(value) {
            // at least one digit,
            // at least one lowercase,
            // at least one uppercase,
            // at least 7 characters
            if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}$/.test(value)) {
                throw new Error('Insecure password');
            }
        }
    },
    role: {
        type: String,
        required: true,
        enum: Object.values(ROLES),
        default: ROLES.REGULAR
    },
    tokens: [String]
}, {
    timestamps: true
});

userSchema.virtual('zones', {
    ref: 'Zone',
    localField: '_id',
    foreignField: 'author'
});

userSchema.methods.toJSON = function() {
    const obj = this.toObject();

    delete obj.password;
    delete obj.tokens;

    return obj;
};

userSchema.methods.generateAuthToken = async function() {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET, {
        expiresIn: '7d'
    });

    user.tokens = user.tokens.concat(token);
    await user.save();

    return token;
};

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email });

    if (!user) {
        throw new Error('Unable to login');
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
        throw new Error('Unable to login');
    }

    return user;
};

userSchema.pre('save', async function(next) {
    const user = this;

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }

    next();
});

userSchema.pre('remove', async function(next) {
    const user = this;
    await Zone.deleteMany({ author: user._id });
    next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;