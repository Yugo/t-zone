const mongoose = require('mongoose');

const dictSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    dictionary: {
        type: mongoose.Schema.Types.Mixed,
        required: true,
        default: []
    }
});

dictSchema.statics.findByName = function(name) {
    return this
        .findOne({ name })
        .select('dictionary -_id');
}

const Dict = mongoose.model('Dict', dictSchema);

module.exports = Dict;