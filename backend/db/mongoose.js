const mongoose = require('mongoose');
const debug = require('debug')('[DATABASE]');

mongoose.connect(process.env.DB_HOST + '/' + process.env.DB_NAME, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
})

const db = mongoose.connection;

db.on('error', error => {
    debug('Connection error:', error.message);
    process.exit(1);
});
db.on('open', () => {
    debug('Connection established');
});