const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const Dict = require('../models/Dict');
const debug = require('debug')('[API:DICTIONARY]');

router.get('/zones', auth, async (req, res) => {
    try {
        const { dictionary } = await Dict.findByName('zones');
        res.send(dictionary);
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

module.exports = router;