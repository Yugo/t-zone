const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const User = require('../models/User');
const debug = require('debug')('[API:USERS]');
const { ROLES } = require('../utils/enums');

router.post('/signup', async (req, res) => {
    req.body.role = ROLES.REGULAR;
    const user = new User(req.body);
    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({ user, token });
    } catch (e) {
        debug(e.message);
        if (/^E11000 duplicate key/.test(e.message)) {
            res.status(400).send('This email is already in use');
        } else {
            res.sendStatus(500);
        }
    }
});

router.post('/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch (error) {
        debug(error.message);
        let status = 500;
        if (error.message === 'Unable to login') {
            status = 401;
        }
        res.status(status).send(error.message);
    }
});

router.get('/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter(token => token !== req.token);
        await req.user.save();
        res.send();
    } catch (e) {
        res.status(500).send();
    }
});

router.get('/logout-all', auth, async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send();
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/', auth, async (req, res) => {
    if (!req.isUserAdmin) {
        return res.sendStatus(403);
    }
    const users = await User.find();
    res.send(users);
});

router.get('/me', auth, async (req, res) => {
    res.send(req.user);
});

router.get('/:id', auth, async (req, res) => {
    if (!req.user || !req.isUserAdmin) return res.sendStatus(403);
    if (req.params.id === req.user.id) return res.send(req.user);

    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.sendStatus(404);
        }
        res.send(user);
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

router.post('/', auth, async (req, res) => {
    if (!req.user || !req.isUserAdmin) return res.sendStatus(403);

    if (!req.body.role || req.user.role !== ROLES.SUPER_USER) { req.body.role = ROLES.REGULAR; }
    const user = new User(req.body);

    try {
        await user.save();
        res.status(201).send(user);
    } catch (e) {
        debug(e.message);
        res.sendStatus(400);
    }
});

router.patch('/me', auth, async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password'];
    const isValidOperation = updates.every(update => allowedUpdates.includes(update));

    if (!isValidOperation) {
        return res.status(400).send('Invalid field update');
    }

    try {
        updates.forEach(update => req.user[update] = req.body[update]);
        await req.user.save();
        res.send(req.user);
    } catch (e) {
        debug(e.message);
        res.sendStatus(400);
    }
});

router.patch('/:id', auth, async (req, res) => {
    if (!req.isUserAdmin) return res.sendStatus(403);

    try {
        const user = await User.findById(req.params.id);
        if (!user) return res.sendStatus(404);

        if (user.role !== ROLES.REGULAR) return res.sendStatus(403);
        const updates = Object.keys(req.body);
        const allowedUpdates = ['name', 'email', 'password'];
        if (req.user.role === ROLES.SUPER_USER) {
            allowedUpdates.push('role');
        }
        const isValidOperation = updates.every(update => allowedUpdates.includes(update));

        if (!isValidOperation) {
            return res.status(400).send('Invalid field update');
        }

        updates.forEach(update => user[update] = req.body[update]);
        await user.save();
        res.send(user);
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

router.delete('/me', auth, async (req, res) => {
    try {
        await req.user.remove();
        res.send(req.user);
    } catch (e) {
        res.status(500).send();
    }
});

router.delete('/:id', auth, async (req, res) => {
    if (!req.isUserAdmin) return res.sendStatus(403);

    try {
        const user = await User.findById(req.params.id);
        if (!user) return res.sendStatus(404);

        if (user.role !== ROLES.REGULAR && req.user.role !== ROLES.SUPER_USER) {
            return res.sendStatus(403);
        }

        await user.remove();
        res.send(user);
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

module.exports = router;
