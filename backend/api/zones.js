const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const Zone = require('../models/Zone');
const debug = require('debug')('[API:ZONES]');
const { ROLES } = require('../utils/enums');

router.get('/', auth, async (req, res) => {
    try {
        if (req.user.role === ROLES.REGULAR) {
            res.send(await Zone.findByAuthorId(req.user.id));
        } else if (req.user.role === ROLES.ADMIN || req.user.role === ROLES.SUPER_USER) {
            res.send(await Zone.find());
        } else {
            return res.sendStatus(403);
        }
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        if (req.user.role === ROLES.REGULAR) {
            res.send(await Zone.findByAuthorId(req.user.id, req.params.id));
        } else if (req.user.role === ROLES.ADMIN || req.user.role === ROLES.SUPER_USER) {
            res.send(await Zone.findById(req.params.id));
        } else {
            return res.sendStatus(403);
        }
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

router.post('/', auth, async (req, res) => {
    req.body.author = req.user.id;
    const zone = new Zone(req.body);
    try {
        await zone.save();
        res.send(zone);
    } catch (e) {
        debug(e.message);
        res.sendStatus(400);
    }
});

router.patch('/:id', auth, async (req, res) => {
    try {
        let zone;
        if (req.user.role === ROLES.REGULAR) {
            zone = await Zone.findByAuthorId(req.user.id, req.params.id);
        } else if (req.user.role === ROLES.ADMIN || req.user.role === ROLES.SUPER_USER) {
            zone = await Zone.findById(req.params.id);
        } else {
            return res.sendStatus(403);
        }

        if (!zone) { return res.sendStatus(404); }

        const updates = Object.keys(req.body);
        const allowedUpdates = ['name', 'city', 'offset'];
        const isValidOperation = updates.every(update => allowedUpdates.includes(update));

        if (!isValidOperation) {
            return res.status(400).send({ error: 'Invalid field update' });
        }

        updates.forEach(update => zone[update] = req.body[update]);
        await zone.save();
        res.send(zone);
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        if (req.user.role === ROLES.REGULAR) {
            const zone = await Zone.findOneAndRemove({
                _id: req.params.id,
                author: req.user.id
            });
            return res.send(zone);
        } else if (req.user.role === ROLES.ADMIN || req.user.role === ROLES.SUPER_USER) {
            return res.send(await Zone.findByIdAndRemove(req.params.id));
        } else {
            return res.sendStatus(403);
        }
    } catch (e) {
        debug(e.message);
        res.sendStatus(500);
    }
});

module.exports = router;
